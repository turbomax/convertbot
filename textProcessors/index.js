const flip = (inText) => {
  const dict = [
    { orig: "a", to: "ɐ" },
    { orig: "b", to: "q" },
    { orig: "c", to: "ɔ" },
    { orig: "d", to: "p" },
    { orig: "e", to: "ǝ" },
    { orig: "f", to: "ɟ" },
    { orig: "g", to: "ƃ" },
    { orig: "h", to: "ɥ" },
    { orig: "i", to: "ᴉ" },
    { orig: "j", to: "ɾ" },
    { orig: "k", to: "ʞ" },
    { orig: "l", to: "l" },
    { orig: "m", to: "ɯ" },
    { orig: "n", to: "u" },
    { orig: "o", to: "o" },
    { orig: "p", to: "d" },
    { orig: "q", to: "b" },
    { orig: "r", to: "ɹ" },
    { orig: "s", to: "s" },
    { orig: "t", to: "ʇ" },
    { orig: "u", to: "n" },
    { orig: "v", to: "ʌ" },
    { orig: "w", to: "ʍ" },
    { orig: "x", to: "x" },
    { orig: "y", to: "ʎ" },
    { orig: "z", to: "z" },
    { orig: ",", to: "'" },
    { orig: "?", to: "¿" },
    { orig: "!", to: "¡" },
  ];

  const inputChars = inText.toLowerCase().split("");
  const transformedChars = inputChars
    .map((c) => {
      const dictelement = dict.find((el) => el.orig === c);
      return dictelement ? dictelement.to : c;
    })
    .reverse();
  const resultString = transformedChars.join("");
  return resultString;
};

module.exports = {
  flip,
};
