require("dotenv").config();
const TG = require("telegram-bot-api");

const textProcessors = require("./textProcessors");
const apiKey = process.env.TELEGRAM_TOKEN;

const api = new TG({
  token: apiKey,
});

api.setMessageProvider(new TG.GetUpdateMessageProvider());

api.start().then(() => {
  console.log("bot running");
});

api.on("update", (update) => {

  const chatId = update.message.chat.id;
  const outMessage = textProcessors.flip(update.message.text);


  api.sendMessage({
    chat_id: chatId,
    text: `<code>${outMessage}</code>`,
    parse_mode: "HTML",
  });
});
